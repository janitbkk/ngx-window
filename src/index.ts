import { CommonModule, isPlatformBrowser } from "@angular/common";
import { 
  NgModule, 
  ModuleWithProviders, 
  InjectionToken, 
  PLATFORM_ID
} from '@angular/core';

import { fakeWindow } from './fake-window';
export { fakeWindow } from './fake-window';

export const WINDOW = new InjectionToken('WindowToken');

export abstract class WindowRef {
  get nativeWindow(): Window | Object {
    throw new Error('Not implemented.');
  }
}

export class BrowserWindowRef extends WindowRef {
  constructor() {
    super();
  }

  get nativeWindow(): Window | Object {
    return window;
  }

  get fakeWindow(): Window | Object {
    return new fakeWindow();
  }
}

@NgModule({
  imports: [CommonModule],
})
export class WindowModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WindowModule,
      providers: [
        { provide: WindowRef, useClass: BrowserWindowRef },
        {
          provide: WINDOW,
          useFactory: windowFactory,
          deps: [ WindowRef, PLATFORM_ID ]
        }
      ]
    };
  }
}

export function windowFactory(browserWindowRef: BrowserWindowRef, platformId: Object): Window | Object {
  if (isPlatformBrowser(platformId)) {
    return browserWindowRef.nativeWindow;
  }
  return browserWindowRef.fakeWindow;
}